cmake_minimum_required(VERSION 3.19)

project(unique_any CXX)

option(ENABLE_UNIQUE_ANY_TESTS "Enable creation of unique_any tests" OFF)


add_subdirectory(unique_any)


if(ENABLE_UNIQUE_ANY_TESTS)
    message(STATUS "Adding unique_any tests")
    enable_testing()
    add_subdirectory(extern/Catch2)
    add_subdirectory(tests)
endif()
