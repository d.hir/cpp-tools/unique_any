#include "dhir/unique_any.hpp"

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <catch2/reporters/catch_reporter_console.hpp>

#include <cstdint>


struct SimpleStruct {
    float x = 3.4f;
    float y = 6.8f;
    friend bool operator==(const SimpleStruct& lhs, const SimpleStruct& rhs);
};

bool operator==(const SimpleStruct& lhs, const SimpleStruct& rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

struct PaddedStruct {
    long l = 1234l;
    char c = 'c';
    friend bool operator==(const PaddedStruct& lhs, const PaddedStruct& rhs);
};

bool operator==(const PaddedStruct& lhs, const PaddedStruct& rhs) {
    return lhs.l == rhs.l && lhs.c == rhs.c;
}

class VTableClass {
public:
    virtual void myfunc() {};
    friend bool operator==(const VTableClass& lhs, const VTableClass& rhs);
private:
    int value = 9;
};

bool operator==(const VTableClass& lhs, const VTableClass& rhs) {
    return lhs.value == rhs.value;
}


class NestedAny {
public:
    friend bool operator==(const NestedAny& lhs, const NestedAny& rhs);
private:
    unsigned int before = 981;
    dhir::unique_any any = dhir::unique_any(7ull);
    unsigned int after = 100;
};

bool operator==(const NestedAny& lhs, const NestedAny& rhs) {
    bool anyeq = lhs.any.get<unsigned long long>() == rhs.any.get<unsigned long long>();
    return lhs.before == rhs.before && lhs.after == rhs.after && anyeq;
}


template<typename T> T value() { return T(); }
template<> float value() { return 3.14f; }
template<> double value() { return 2.71828; }
template<> std::int8_t value() { return std::int8_t{ -8 }; }
template<> std::uint8_t value() { return std::uint8_t{ 8 }; }
template<> std::int16_t value() { return std::int16_t{ -16 }; }
template<> std::uint16_t value() { return std::uint16_t{ 16 }; }
template<> std::int32_t value() { return std::int32_t{ -32 }; }
template<> std::uint32_t value() { return std::uint32_t{ 32 }; }
template<> std::int64_t value() { return std::int64_t{ -64 }; }
template<> std::uint64_t value() { return std::uint64_t{ 64 }; }


TEST_CASE("empty unique_any", "[edge]") {
    dhir::unique_any any;
    SECTION("default constructed unique_any objects are empty") {
        REQUIRE(any.empty());
    }
    SECTION("moving empty unique_any is handled correctly") {
        dhir::unique_any other(std::move(any));
        REQUIRE(other.empty());
        REQUIRE(&other != &any);
    }
}


TEMPLATE_TEST_CASE("basic unique_any", "[basic]", float, double) {
    dhir::unique_any any(value<TestType>());
    SECTION("unique_any is not empty") {
        REQUIRE_FALSE(any.empty());
    }
    SECTION("retrieving gives the correct value") {
        REQUIRE_THAT(any.get<TestType>(), Catch::Matchers::WithinULP(value<TestType>(), 0));
    }
    SECTION("moving to different unique_any is handled correctly") {
        dhir::unique_any other(std::move(any));
        REQUIRE_FALSE(other.empty());
        REQUIRE_THAT(other.get<TestType>(), Catch::Matchers::WithinULP(value<TestType>(), 0));
    }
}


TEMPLATE_TEST_CASE("basic unique_any", "[basic]", std::int8_t, std::uint8_t, std::int16_t, std::uint16_t, std::int32_t, std::uint32_t, std::int64_t, std::uint64_t) {
    dhir::unique_any any(value<TestType>());
    SECTION("unique_any is not empty") {
        REQUIRE_FALSE(any.empty());
    }
    SECTION("retrieving gives the correct value") {
        REQUIRE(any.get<TestType>() == value<TestType>());
    }
    SECTION("moving to different unique_any is handled correctly") {
        dhir::unique_any other(std::move(any));
        REQUIRE_FALSE(other.empty());
        REQUIRE(other.get<TestType>() == value<TestType>());
    }
}


TEMPLATE_TEST_CASE("basic unique_any", "[basic]", SimpleStruct, PaddedStruct, VTableClass, NestedAny) {
    dhir::unique_any any(value<TestType>());
    SECTION("unique_any is not empty") {
        REQUIRE_FALSE(any.empty());
    }
    SECTION("retrieving gives the correct value") {
        REQUIRE(any.get<TestType>() == value<TestType>());
    }
    SECTION("moving to different unique_any is handled correctly") {
        dhir::unique_any other(std::move(any));
        REQUIRE_FALSE(other.empty());
        REQUIRE(other.get<TestType>() == value<TestType>());
    }
}


TEST_CASE("std::string unique_any", "[basic]") {
    dhir::unique_any any(std::string("pi = 3"));
    SECTION("std::string unique_any is not empty") {
        REQUIRE_FALSE(any.empty());
    }
    SECTION("retrieving a std::string gives the correct value") {
        REQUIRE(any.get<std::string>() == "pi = 3");
        REQUIRE(any.get<const std::string>() == "pi = 3");
    }
    SECTION("moving to a different unique_any is handled correctly") {
        dhir::unique_any other(std::move(any));
        REQUIRE_FALSE(other.empty());
        REQUIRE(other.get<std::string>() == "pi = 3");
        REQUIRE(other.get<const std::string>() == "pi = 3");
    }
    std::string str("e = 2");
    std::string& str_ref = str;
    dhir::unique_any any_ref(str_ref);
    SECTION("std::string unique_any is not empty") {
        REQUIRE_FALSE(any_ref.empty());
    }
    SECTION("retrieving a std::string gives the correct value") {
        REQUIRE(any_ref.get<std::string>() == "e = 2");
        REQUIRE(any_ref.get<const std::string>() == "e = 2");
    }
    SECTION("moving to a different unique_any is handled correctly") {
        dhir::unique_any other(std::move(any_ref));
        REQUIRE_FALSE(other.empty());
        REQUIRE(other.get<std::string>() == "e = 2");
        REQUIRE(other.get<const std::string>() == "e = 2");
    }
    const std::string c_str("g = 10");
    const std::string& c_str_ref = c_str;
    dhir::unique_any any_c_ref(c_str_ref);
    SECTION("std::string unique_any is not empty") {
        REQUIRE_FALSE(any_c_ref.empty());
    }
    SECTION("retrieving a std::string gives the correct value") {
        REQUIRE(any_c_ref.get<std::string>() == "g = 10");
        REQUIRE(any_c_ref.get<const std::string>() == "g = 10");
    }
    SECTION("moving to a different unique_any is handled correctly") {
        dhir::unique_any other(std::move(any_c_ref));
        REQUIRE_FALSE(other.empty());
        REQUIRE(other.get<std::string>() == "g = 10");
        REQUIRE(other.get<const std::string>() == "g = 10");
    }
}