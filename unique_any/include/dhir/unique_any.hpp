#pragma once

#include <memory>
#include <string>
#include <typeinfo>
#include <utility>

namespace dhir {


class unique_any;

template<typename T>
concept not_unique_any = !std::is_base_of<unique_any, std::remove_cv<T>>::value;

class UniqueAnyEmpty : public std::exception {
public:
    UniqueAnyEmpty() noexcept = default;

    const char * what() const noexcept override {
        return msg.c_str();
    }

private:
    std::string msg = "Invalid operation: No value set in unique_any";
};

class UniqueAnyInvalidAccess : public std::exception {
public:
    UniqueAnyInvalidAccess() noexcept = default;

    const char * what() const noexcept override {
        return msg.c_str();
    }

private:
    std::string msg = "Invalid operation: Set value is of different type than accessed";
};

class unique_any {
private:
    struct _Base {
        virtual ~_Base() = default;
    };

    template<typename T>
    struct _Holder : public _Base {
        T value;

        template<typename U>
        _Holder(U&& value);
        _Holder(const _Holder& other) = delete;
        _Holder& operator=(const _Holder& other) = delete;
    };

    std::unique_ptr<_Base> storage;

public:
    unique_any() = default;
    ~unique_any() = default;
    template<not_unique_any T>
    unique_any(T&& value);
    unique_any(const unique_any& other) = delete;
    unique_any& operator=(const unique_any& other) = delete;
    unique_any(unique_any&& other) = default;
    unique_any& operator=(unique_any&& other) = default;

    template<typename T>
    T& get();

    template<typename T>
    const T& get() const;

    inline bool empty() const noexcept;
};

template<typename T>
template<typename U>
unique_any::_Holder<T>::_Holder(U&& value) : value(std::forward<U>(value)) {};

template<not_unique_any T>
unique_any::unique_any(T&& value) : storage(std::make_unique<unique_any::_Holder<std::remove_cv_t<std::remove_reference_t<T>>>>(std::forward<T>(value))) {};

template<typename T>
T& unique_any::get() {
    _Base * bp = storage.get();
    if (bp == nullptr) {
        throw UniqueAnyEmpty();
    }
    _Holder<T> * hp = dynamic_cast<_Holder<T> *>(bp);
    if (hp != nullptr) {
        return hp->value;
    }
    _Holder<std::remove_cv_t<T>> * bare_hp = dynamic_cast<_Holder<std::remove_cv_t<T>> *>(bp);
    if (bare_hp != nullptr) {
        return bare_hp->value;
    }
    throw UniqueAnyInvalidAccess();
}

template<typename T>
const T& unique_any::get() const {
    const _Base * bp = storage.get();
    if (bp == nullptr) {
        throw UniqueAnyEmpty();
    }
    const _Holder<T> * hp = dynamic_cast<const _Holder<T> *>(bp);
    if (hp != nullptr) {
        return hp->value;
    }
    const _Holder<std::remove_cv_t<T>> * bare_hp = dynamic_cast<const _Holder<std::remove_cv_t<T>> *>(bp);
    if (bare_hp != nullptr) {
        return bare_hp->value;
    }
    throw UniqueAnyInvalidAccess();
}

inline bool unique_any::empty() const noexcept {
    return !storage;
}


}  // namespace dhir
